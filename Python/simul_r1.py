# %% codecell
import numpy as np
import pandas as pd
# import matplotlib.pyplot as plt
# import seaborn as sns
import plotly.graph_objects as go
import plotly.express as px
# import plotly.figure_factory as ff
from tqdm import tqdm
# %% markdown

# %% codecell
width, precision = 8, 5   # for formatted strings Python>=3.6
gam = 20                  # shape parameter of Gamma distributions
np.random.seed(142)       # why 142 and not 42 ?
mu1, mu2 = np.random.uniform(0, 2, size=2)
n = 100                   # sample size
# %% markdown

# %% codecell
X1 = mu1 + np.random.gamma(gam, size=n)
X2 = mu2 + np.random.gamma(gam, size=n)
# %% markdown
#
# %% codecell
X1_mean = X1.mean()
X2_mean = X2.mean()

print(f'X1 mean: {X1_mean:{width}.{precision}} / X2 mean: {X2_mean:{width}.{precision}}')
if (X1_mean - X2_mean < 0):
    print('mu1 less than mu2?')
else:
    print('mu1 greater than mu2?')
# %% markdown
#
# %% codecell
def simu(gam, mu1,  mu2, n):
    """Generator for differences between sample means of translated Gamma samples of length
    `n`

    Parameters
    ----------
    gam : float,
        shape parameter of Gamma distributions
    mu1, mu2 : float,
        translation parameters for the two Gamma samples
    n : int
        length of the two Gamma samples

    Returns
    -------
    float
        Difference between sample means

    """
    while True:
        X1 = mu1 + np.random.gamma(gam, size=n)
        X2 = mu2 + np.random.gamma(gam, size=n)
        yield(X1.mean() - X2.mean())
# %% codecell
r=100
deltas = np.fromiter(simu(gam, mu1, mu2, n), 'double', r)
%timeit np.fromiter(simu(gam, mu1, mu2, n), 'double', r)
pd_deltas = pd.Series(deltas)
pd_deltas_sign = pd_deltas < 0
pd_deltas_sign.value_counts()
# %% markdown

# %% codecell
pd_deltas
# %% markdown

# %% codecell
r = 10000
deltas_ref = np.array([
    np.random.gamma(gam, size=n).mean() - np.random.gamma(gam, size=n).mean()
    for i in range(r)
])
# %% codecell
# sns.distplot(a=deltas_ref);


# %% markdown
What is a _p-value_?
# %% codecell
print(f'delta: {X1_mean - X2_mean:{width}.{precision}}')
print(f'p-value: {(deltas_ref < (X1_mean - X2_mean)).mean():{width}.{precision}}')
# %% codecell
n = 10000

X1 = mu1 + np.random.gamma(gam, size=n)
X2 = mu2 + np.random.gamma(gam, size=n)
X1_mean = X1.mean()
X2_mean = X2.mean()

# %% codecell
print(f'X1 mean: {X1_mean:{width}.{precision}} / X2 mean: {X2_mean:{width}.{precision}}')

if (X1_mean - X2_mean < 0):
    print('mu1 less than mu2?')
else:
    print('mu1 greater than mu2?')
# %% codecell
# r = 1000
# deltas_ref = np.array([ np.random.gamma(gam, size=n).mean() - np.random.gamma(gam, size=n).mean() for i in range(r)])
# sns.distplot(a=deltas_ref, );

# %% markdown
# No need to play with list comprehensions
# This looks faster (using %timeit)
# %% codecell
r = 10000
deltas_ref = np.random.gamma(gam, size=(n, r)).mean(axis=0) - \
    np.random.gamma(gam, size=(n, r)).mean(axis=0)

# %% codecell
# fig = go.Figure()

# fig.add_trace(go.Histogram
fig = ff.create_distplot([deltas_ref, deltas_ref],
                         group_labels=['bins=.05', 'bins=.1'],
                         bin_size=[.05, .1]
                         )


fig.update_layout(
    title_text=f'Distplot of deltas between two Gamma samples means of size {n}, {r} experiments'
)

fig.show()
# %% markdown
# Some explanations about p-values
# %% codecell
print(f'delta: {(X1_mean - X2_mean):{width}.{precision}}')
print(f'p-value: {(deltas_ref < (X1_mean - X2_mean)).mean():{width}.{precision}}')
# %% codecell
Xboot = np.concatenate([X1, X2])
# %% codecell
def boot_permute():
    """Short summary.

    Returns
    -------
    type
        Description of returned object.

    """
    while True:
        Xperm = np.random.permutation(Xboot)
        yield(Xperm[:n].mean()-Xperm[-n:].mean())

# The first argument of `fromiter` is an iterable (a generator fits there). `fromiter`
# returns a `numpy array`. Second argument defines the `dtype`  of the array.
# Third argument, when used, allows to preallocate the array (otherwise some doubling trick
# may be used).
# %% codecell
deltas_rep_permute = np.fromiter(boot_permute(), 'double', r)
sns.distplot(a = deltas_rep_permute)

# %% codecell
fig = ff.create_distplot([deltas_ref, deltas_rep_permute],
                         group_labels=['originals', 'permutation resampling'],
                         bin_size=[.05, .05],
                         curve_type='normal'
                         )

fig.update_layout(
    title_text=f'Distplot of deltas between two translated Gamma samples of length{n}, with resampling'
)

fig.show()
# %% codecell
def boot_resample():
    while True:
        yield(np.random.choice(Xboot, n, replace=True).mean() -
              np.random.choice(Xboot, n, replace=True).mean())
# %% codecell
# %% codecell
deltas_rep_resample = np.fromiter(boot_resample(), 'double', r)
sns.distplot(a = deltas_rep_resample);

# %% codecell
fig = ff.create_distplot([deltas_ref,
                          deltas_rep_permute,
                          deltas_rep_resample],
                          group_labels=['originals',
                                        'permute',
                                        'resample'],
                          bin_size=[.01, 0.01, .01],
#                           show_hist=False,
#                          curve_type='normal',
                         show_curve=False
)

fig.update_layout(
    title_text=f'Distplot of resampled deltas between two translated Gamma samples of length{n}, with resampling'
)

fig.show()
# %% codecell
# deltas_rep = np.fromiter((np.random.choice(Xboot, n, replace=True).mean() -
#       np.random.choice(Xboot, n, replace=True).mean()), 'double', r)
