# %% markdown
# # Permutations aléatoires
#
# On considère $\Omega$  l'ensemble des permutations de $\{1, \ldots, n\}$. On munit  $(\Omega, \mathcal{F}=2^{\Omega})$ de la loi uniforme  $P$ sur $\Omega$.
# Un tirage selon $P$ donne une _permutation aléatoire_. On s'intéresse aux propriétés de
# ces permutations aléatoires.
#
# %% codecell
import numpy as np
import scipy.stats as ss
import pandas as pd # usual suspects

import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from tqdm import tqdm    # progress bars
from numba import jit, njit  # loops on steroids
import numba

import warnings

# %% codecell
rng = np.random.default_rng(42)

# %% codecell
params = {'n': 1000,  # size of set permutations are acting on
          'B': 10000   # number of samples
          }
n, B = params.values()

%load_ext heat

#warnings.filterwarnings('ignore')
warnings.filterwarnings(action='once')
# %% markdown
# ## Engendrer des permutations aléatoires
#
# On peut engendrer une permutation aléatoire sur $0, \ldots, n-1$ à partir
# de tirages aléatoires indépendants uniformes sur $0, \ldots, n-1$, $0, \ldots, n-2$, $0, \ldots, 1$. On attribue le code qui suit à Knuth.
#
# On note que l'entropie binaire de la source $\log_2 n! =\sum_{i=2}^n \log_2 (i)$
# est l'entropie binaire de la loi uniforme sur l'ensemble des permutations sur $0, \ldots, n-1$. Comme on peut regarder l'algorithme de génération aléatoire comme un algorithme qui transforme une source de bits aléatoires en une réalisation d'une loi de probabilité, l'entropie de la source binaire doit être au moins aussi grande que celle de la loi à simuler.
#
# La méthode ci-dessous est optimale du point de vue de l'usage de la source aléatoire.
# %% codecell
# @jit(forceobj=True)
# @jit()
def gen_permutation(n=10, rng=np.random.default_rng(42)):
    """Generate a random permutation acting on 0, ..., n-1.

    Parameters
    ----------
    n : int

    Returns
    -------
    type
        numpy array of length n containing a random permutation of 0, ..., n-1
    """
    a = list(range(n)) # np.arange(0, n, dtype='int')   # start from a sorted array
    for i in range(n-1, 0, -1):
        j = np.random.randint(0, i+1) #rng.integers(0, i+1)  # uniform in 0,...,i
        a[i], a[j] = a[j], a[i]   # swap, transposition
    return np.array(a)

_ = gen_permutation(10)  # running-in execution
# %% codecell
g0 = gen_permutation
g1 = jit(gen_permutation, forceobj=True)
g2 = jit(gen_permutation)

_ = g1(10)
_ = g2(10)


# %% codecell
# %%heat
#
# import numpy as np
#
# def gen_permutation(n=10, rng=np.random.default_rng(42)):
#     """Generate a random permutation acting on 0, ..., n-1.
#
#     Parameters
#     ----------
#     n : int
#
#     Returns
#     -------
#     type
#         numpy array of length n containing a random permutation of 0, ..., n-1
#     """
#     a = list(range(n)) # np.arange(0, n, dtype='int')   # start from a sorted array
#     for i in range(n-1, 0, -1):
#         j = np.random.randint(0, i+1) #rng.integers(0, i+1)  # uniform in 0,...,i
#         a[i], a[j] = a[j], a[i]   # swap, transposition
#     return np.array(a)
#
# aa = gen_permutation(10000)
# %% markdown
# Si `numpy` le fait déjà, il est difficile de faire vraiment plus efficace.

# %% markdown
# Notre problème d'efficacité provient (entre autres) de fait que nous appelons
# le générateur de nombres aléatoires à chaque itération.
# Dans la version suivante, on appelle une fois `rng.random` et on multiplie
# le résultat,  un vecteur de réalisations réputées indépendantes de la loi uniforme sur $[0,1]$,
# par $n-1, n-2, \ldots, 1$. On cherche à gagner en efficacité.
# On gagne surtout en obscurité.
# %% codecell

# @jit(forceobj=True)
def gen_permutation_speedy(n=10, rng=np.random.default_rng(42)):
    """Generate a random permutation acting on 0, ..., n-1.

    Parameters
    ----------
    n : int

    Returns
    -------
    type
        numpy array of length n containing a random permutation of 0, ..., n-1
    """
    a = list(range(n))

    for i, j in zip(range(1,n), np.array(np.floor(np.random.rand(n-1) * list(range(1,n))), dtype='int')):
        a[i], a[j] = a[j], a[i]   # swap, transposition
    return np.array(a, dtype='int')

g3 = gen_permutation_speedy
g4 = jit(gen_permutation_speedy)
g5 = jit(gen_permutation_speedy, forceobj=True)

_ = g4(10)   # running-in executions
_ = g5(10)
# %% codecell
%timeit g0(10000)   # native
%timeit g1(10000)   # no good
%timeit g2(10000)   # halves run time
%timeit g3(10000)   # native improved by a factor of 10
%timeit g4(10000)   # hurts !
%timeit g5(10000)   # hurts !
%timeit rng.shuffle(np.arange(10000))  # much better


# %% codecell
# %%heat
#
# import numpy as np
#
# def gen_permutation_speedy(n=10, rng=np.random.default_rng(42)):
#     """Generate a random permutation acting on 0, ..., n-1.
#
#     Parameters
#     ----------
#     n : int
#
#     Returns
#     -------
#     type
#         numpy array of length n containing a random permutation of 0, ..., n-1
#     """
#     a = list(range(n))
#
#     for i, j in zip(range(1,n), np.array(np.floor(np.random.rand(n-1) * list(range(1,n))), dtype='int')):
#         a[i], a[j] = a[j], a[i]   # swap, transposition
#     return np.array(a, dtype='int')
#
#
# aa = gen_permutation_speedy(100000)
# %% markdown
# ## Compter les cycles d'une permutation
# La compilation par `numba.jit` permet ici de rendre le code plus efficace
# Les deux boucles imbriquées sont lentes en Python pur.
# La ran
# %% codecell
def slow_count_cycles(a):
    """Computes number of cycles in permutation of 0, ..., n-1.

    Parameters
    ----------
    a : numpy array of integers  (np.int32)
        `a` contains a permutation of 0, ..., len(a)-1

    Returns
    -------
    int
        Number of cycles in permutation defined by `a`.

    """
    n = a.shape[0]
    b = np.ones(n, dtype=np.int8)
    for i in range(n):
        if b[i]: # cycle anchor at i
            j = a[i]
            while j != i:
                b[j] = 0 # not a cycle anchor
                j = a[j]

    return np.sum(b, dtype=np.int32)

# %% codecell
# %%heat
#
# import numpy as np
#
# def slow_count_cycles(a):
#     """Computes number of cycles in permutation of 0, ..., n-1.
#
#     Parameters
#     ----------
#     a : numpy array of integers  (np.int32)
#         `a` contains a permutation of 0, ..., len(a)-1
#
#     Returns
#     -------
#     int
#         Number of cycles in permutation defined by `a`.
#
#     """
#     n = a.shape[0]
#     b = np.ones(n, dtype=np.int8)
#     for i in range(n):
#         if b[i]: # cycle anchor at i
#             j = a[i]
#             while j != i:
#                 b[j] = 0 # not a cycle anchor
#                 j = a[j]
#
#     return np.sum(b, dtype=np.int32)
#
# a = np.arange(100000, dtype=np.int32)
# np.random.default_rng(42).shuffle(a)
#
# _ = slow_count_cycles(a)
# %% codecell
@jit(numba.int32(numba.int32[:]))   # comment out if you want the slow path
def count_cycles(a):
    """Computes number of cycles in permutation of 0, ..., n-1.

    Parameters
    ----------
    a : numpy array of integers  (np.int32)
        `a` contains a permutation of 0, ..., len(a)-1

    Returns
    -------
    int
        Number of cycles in permutation defined by `a`.

    """
    n = a.shape[0]
    b = np.ones(n, dtype=np.int8)
    for i in range(n):
        if b[i]: # cycle anchor at i
            j = a[i]
            while j != i:
                b[j] = 0 # not a cycle anchor
                j = a[j]

    return np.sum(b, dtype=np.int32)

# %% codecell
cc0 = slow_count_cycles
cc1 = count_cycles

a = np.arange(100000, dtype=np.int32)
rng.shuffle(a)

%timeit cc0(a)   # slow
%timeit cc1(a)   # faster thanks to numba compilation


# %% markdown
# ## Engendrer un échantillon de nombre de cycles
#
# Fonction `tqdm` du package `tqdm`. On peut l'appliquer à un itérateur.
# La barre de progression est mise à jour lors de chaque invocation de l'itérateur (ici une itération de la boucle `for`).
#
# La fonction `shuffle` de [`numpy.random`](https://numpy.org/doc/stable/reference/random/index.html)
#  réalise une permutation en place du tableau fourni en argument.
# %% codecell

@jit()
def gen_cycle_numbers(n=10, size=100, seed=42):
    """Generates number of cycles in random permutation over `0,...,n-1`, `size`  times

    Args:
        n (int, optional): size of sets permutations are acting on.
        size (int, optional): number of replications/sample size.
        rng (... , optional): a random generator ...

    Returns:
        Array of B integers: each entry is the number of cycles in a random permutation
    """
    shuffler=np.random.default_rng(seed).shuffle
    r = np.zeros(size, dtype=np.int32)
    a = np.arange(n , dtype=np.int32)
    for k in tqdm(range(size)):
        shuffler(a)          # permutation in place
        r[k] = count_cycles(a)  # play it with different levels of steroids
    return r
# %% codecell
help(np.random.default_rng(42).shuffle)
gen_cycle_numbers()
# %% markdown
# On engendre `B=10000` réalisations du nombre de cycles d'une permutation aléatoire pour `n=1 000, 10 000, 100 000`.
#
# On range les trois échantillons dans un dictionnaire indexé par les valeurs de `n`.
# %% codecell
simulations = {
    n: gen_cycle_numbers(np.int32(n), np.int32(10000))
    for n in np.logspace(3, 5, endpoint=True, num=3, base=10, dtype=np.int32)
}


# %% markdown
# On pourrait déterminer à la main la fréquence de chaque nombre de cycles dans nos trois échantillons. La méthode `value_counts` de la classe `Series` du package [`pandas`](https://pandas.pydata.org/pandas-docs/stable/reference/) fournit une solution clé en main.
# %% codecell
df = pd.DataFrame({str(n): pd.Series(s).value_counts()
                     for n, s in simulations.items()})

df.head()
# %% markdown
# Les trois colonnes ne sont pas typées de la même façon. Comment expliquer cette observation ? Comment typer les trois colonnes de façon homogène ?
# %% markdown
# Les trois échantillons définissent chacun une loi de probabilité dite _empirique_ (définie à partir des données). On peut visualiser ces trois distributions de plusieurs manières.
#
# Dans un premier temps, on dessine un diagramme en batons (_barplot_), ce qui est une façon de réaliser à la main un histogramme.
#
# On utilise le package [https://plotly.com/python/](https://plotly.com/python/) qui suit les principes de _Grammar of graphics_.
# %% codecell
fig = go.Figure()

for c in df.columns:
    fig.add_trace(go.Bar(x=df.index, y=df[c], name=f'n={c}'))

fig.update_layout(
    title_text=f'Cyclic decomposition of random permutations, {B} repetitions', # title of plot
    xaxis_title_text='Number of cycles', # xaxis label
    yaxis_title_text='Count' #, # yaxis label
)
fig.show()

# %% markdown
# ##  Center and normalize cycle counts
# %% codecell
def Hn(n):
    return np.sum(1./np.arange(1, n+1))
# %% codecell
def Hn2(n):
    return np.sum(1./np.arange(1, n+1)**2)
# %% markdown
# Comparing empirical mean and computed expectation
#
# %% markdown
# Comparing empirical variance and computed variance
#
# %% codecell
fig = go.Figure()

for c in df.columns:
    n = int(c)
    fig.add_trace(go.Bar(x=(df.index-Hn(n))/np.sqrt(Hn(n) - Hn2(n)), y=df[c], name=f'n={c}'))

fig.update_layout(
    title_text=f'Cyclic decomposition of random permutations, {B} repetitions', # title of plot
    xaxis_title_text='Number of cycles', # xaxis label
    yaxis_title_text='Count' #, # yaxis label
)

fig.update_layout(barmode='overlay')
# Reduce opacity to see both histograms
fig.update_traces(opacity=0.55)

fig.show()

# %% markdown
# ## Fonctions de répartition empiriques
# %% codecell
# Comparing distribution functions
def ecdf(sample):
    """Computes the Empirical Cumulative Distribution Function of `sample`

    Parameters
    ----------
    sample : 1d-ndarray

    Returns
    -------
    pandas.Series
        An index sorted Series indexed by values in `sample` and
        values given by the cumulative frequencies in `sample`
    """
    x = pd.Series(sample)\
          .value_counts()\
          .sort_index()
    ecdf = np.cumsum(x)/np.sum(x)
    return pd.Series(ecdf, index=x.index)

def ecdf_std(sample):
    """Computes the Empirical Cumulative Distribution Function of
    centered and standardized `sample`.

    Parameters
    ----------
    sample : 1d-ndarray

    Returns
    -------
    pandas.DataFrame
        A DataFrame containing a column `x` for the centered annd
        standardized values in `sample`
        and column `ecdf` for the cumulative frequencies
    """
    return ecdf((sample-np.mean(sample))/np.std(sample))

# %% codecell
spam = {str(n): ecdf_std(s) for n, s in simulations.items()}

fig = go.Figure()

for n, cdf in spam.items():
    fig.add_trace(go.Line(x=cdf.index, y=cdf, line_shape='hv', name=f'n: {n}'))


fig.update_layout(
    title_text=f'CDF of normalized number of cycles in random permutations, {B} repetitions', # title of plot
    xaxis_title_text='Centered and normalized number of cycles', # xaxis label
    yaxis_title_text='Cumulative Distribution Function' #, # yaxis label
)

# %% codecell
fig = go.Figure()

for n, cdf in spam.items():
    fig.add_trace(go.Line(x=cdf.index, y=np.sqrt(np.log(int(n)))* (cdf- ss.norm().cdf(cdf.index)), line_shape='hv', name=f'n: {n}'))

fig.update_layout(
    title_text=f'Standardized centered CDF of normalized number of cycles in random permutations, {B} repetitions', # title of plot
    xaxis_title_text='Centered and normalized number of cycles', # xaxis label
    yaxis_title_text='Cumulative Distribution Function' #, # yaxis label
)
# %% markdown
# ## Statistique du nombre de points fixes d'une permutation
# %% codecell
# Distribution du nombre de points fixes


def gen_fixed_points_number(n=10, B=100, rng=np.random.default_rng(42)):
    """Generates number of fixed points in random permutation

    Args:
        n (int, optional): size of sets permutations are acting on. Defaults to 10.
        B (int, optional): number of replications/sample size. Defaults to 100.

    Returns:
        Array of B integers: each entry is the number of fixed points in a random permutation over 1...n
    """
    r = np.zeros(B, dtype=np.int)  # result
    base =  np.arange(n)           # gold standard
    for k in tqdm(range(B)):
        a = rng.permutation(n)
        r[k] = np.sum(a==base)
    return r
# %% codecell
n = int(n)
base =  np.arange(n)
# %% codecell
u = np.array(
    [np.sum(rng.permutation(n)==base) for _ in tqdm(range(B))]
)
# %% codecell
fpsample = gen_fixed_points_number(n=1000, B=1000)

simulations_pointsfixes = {
    n: gen_fixed_points_number(n=1000, B=10000, rng=rng)
    for n in np.logspace(3, 5, endpoint=True, num=3, base=10, dtype=np.int)
}


df_pointsfixes = pd.DataFrame({str(n): pd.Series(s).value_counts()
                     for n, s in simulations_pointsfixes.items()})

# %% codecell
fig = go.Figure()

for c in df_pointsfixes.columns:
    fig.add_trace(go.Bar(x=df_pointsfixes.index, y=df_pointsfixes[c], name=f'n={c}'))

fig.add_trace(go.Bar(x=df_pointsfixes.index, y=B*ss.poisson.pmf(df_pointsfixes.index, 1),
              name='Poisson(1)'))

fig.update_layout(
    title_text=f'Number of fixed points in random permutations over 1, ..., n, {B} repetitions', # title of plot
    xaxis_title_text='Number of fixed points', # xaxis label
    yaxis_title_text='Count' #, # yaxis label
)
fig.show()

# %% codecell
