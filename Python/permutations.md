

```python
# %%
import ipsedixit   # Python parle Latin
import timeit
import numpy as np
import scipy as sc
import re
from scipy import stats
# from scipy.stats import randint
# from scipy.stats import uniform
from numpy.random import random
from numpy.random import randint
from numpy.random import multinomial

# %%
```

# Permutations aléatoires

## Engendrer une permutation aléatoire par la méthode décrite par Knuth

En pseudo-code l'algorithme de Knuth s'écrit à peu près comme:

```{python, evaluate=False}
# %%
n = 100
seq = list(range(1, n+1))  # seqleau des entiers de 1 à n

for i in range(n-1):
    j  = random integer between i and n-1 # n-1 included
    seq[i], seq[j] = seq[j], seq[i]
# %%
```

En Python cela donne:

```python
# %%
n = 100
seq = list(range(1, n+1))  # seqleau des entiers de 1 à n

for i in range(n-1):
    j  = randint(low=i, high=n)
    seq[i], seq[j] = seq[j], seq[i]
# %%
```

On peut permuter n'importe quel tableau indexé à partir de $0$.

```python
# %%
generator = ipsedixit.Generator("caesar")
tokenizer =  re.compile('\w+', flags=0)
seq = tokenizer.findall(generator.paragraphs(n=1, min=2, max=4, seed=None)[0])
seq_copy = seq.copy()
all([w == v for w, v in zip(seq, seq_copy)])

for i in range(len(seq)-1):
    j  = randint(low=i, high=len(seq))
    seq[i], seq[j] = seq[j], seq[i]

all([w == v for w, v in zip(seq, seq_copy)])  # dans le désordre

all([w == v for w, v in zip(sorted(seq), sorted(seq_copy))])  # Les memes mots
# %%
```

Voir [mtrand.pyx](https://github.com/bashtage/numpy/blob/master/numpy/random/mtrand/mtrand.pyx)
pour la mise en oeuvre véritable.

La mise en oeuvre la plus fidèle est
la fonction `shuffle` ci-dessous... Cette mise en oeuvre est lente.
Elle effectue un appel fonctionnel au générateur de nombres entiers
pour chaque cellule du seqleau. On obtient un générateur de
qualité équivalente en engendrant en un seul appel
$n$ flottants distribués uniformément sur $[0,1]$
et en transformant ces flottants aléatoires en entiers aléatoires
distribués sur les plages pertinentes grace à des opérations vectorielles.



```python
# %%
def shuffle(seq):
    n = len(seq)
    r = np.arange(n) + np.array(random(n) *
            np.arange(n, stop=0, step=-1), dtype=sc.int64)
    for i, j in enumerate(r):
        seq[i], seq[j] = seq[j], seq[i]
    return seq

def shuffle_(seq):
    n = len(seq)
    r = random(n)
    for i, x in enumerate(r):
        j = i + int(x*(n-i))
        seq[i], seq[j] = seq[j], seq[i]
    return seq

def shuffle__(seq):
    n = len(seq)
    for i in range(n-1):
        j  = randint(low=i, high=n)
        seq[i], seq[j] = seq[j], seq[i]
    return seq
# %%
```

## Itérateurs qui engendrent des permutations d'un objet initial

```python
# %%
class Permutation(object):
    """docstring for Permutation."""
    def __init__(self, arg):
        self.seq = list(arg)

    def __repr__(self):
        return 'Permutation {0}'.format(reprlib.repr(self.seq))

    def __iter__(self):
        return PermutationIterator(self.seq)

class PermutationIterator(object):
    """docstring for PermutationIterator."""
    def __init__(self, seq):
        # super(PermutationIterator, self).__init__()
        self.perm = list(seq)

    def __next__(self):
        p = shuffle(self.perm)
        self.perm = p
        return p

    def __iter__(self):
        return self
# %%
```

## Memes idées avec des générateurs

```python
# %%
class PermutationGenerator(object):
    """docstring for PermutationGenerator"""
    def __init__(self, arg):
        self.seq = list(arg)
        self.n = len(self.seq)

    def __repr__(self):
        return 'Permutation {0}'.format(reprlib.repr(self.seq))

    def __iter__(self):
        '''Generator function for permutations
        '''
        while True:
            r = random(self.n)
            for i, x in enumerate(r):
                j = i + int(x*(self.n-i)) # uniform in i..n-1
                self.seq[i], self.seq[j] = self.seq[j], self.seq[i]
            yield self.seq
# %%
```

## Avec un générateurs de générateurs !

```python
# %%
class PermutationGeneratorLazy():
    """Generates permutation of an initial sequence
    """
    def __init__(self, arg):
        self.seq = list(arg)
        self.n = len(arg)

    def __repr__(self):
        return reprlib.repr(self.seq)

    def foo(self, i):
        j = randint(i, self.n)
        self.seq[i], self.seq[j] = self.seq[j], self.seq[i]
        return self.seq[i]

    def __iter__(self):
        while True:
            yield (self.foo(i) for i in range(self.n))
# %%
```


```python
# %%
piggy = tokenizer.findall(generator.paragraphs(1, min=2, max=4, seed=None)[0])
julius = iter(PermutationGeneratorLazy(piggy))
for _ in range(5):
    print(list(next(august)))
# %%
```

## Benchmarking

Le  générateur de  générateurs est  (vraiment) très lent.


```python
# %%
%timeit list(next(august))   ## Ecriture pythonique
# 131 µs ± 372 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)

jules = list(next(august))
%timeit shuffle(jules)       ##
# 17.4 µs ± 228 ns per loop (mean ± std. dev. of 7 runs, 100000 loops each)

%timeit np.random.shuffle(jules)  ## Permutations officielles
# 1.85 µs ± 3.96 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
# %%
```

## Engendrer une permutation aléatoire via la méthode suggérée dans l'énoncé

```python
# %%
def permutation(n=10):
    """renvoie une permutation aléatoire de [1,...,n] """
    pasvu, res = {k:True for k in range(1, n+1)}, {}
    for k in range(1, n+1):
        while True:
            i = randint(1, n+1)
            if pasvu[i]:
                pasvu[i] = False
                res[k] = i
                break
    return res

%timeit permutation(len(jules))
# 461 µs ± 5.78 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
# %%
```


# Écrire un programme qui simule un lancer de dé  à 6 faces non biaisé.


```python
# %%
lance_de = sc.stats.randint(low=1, high=7).rvs
lance_de(size=10)
# %%
```

```python
# %%
from functools import partial
lance_de = partial(sc.stats.randint.rvs, low=1, high=7)
lance_de(size=10)
lance_de(10)
# %%
```


```python
# %%
class De():
    """docstring for de: simulation de dé à k=6 faces non biaisé."""
    def __init__(self, arg=6):
        self.faces = int(arg)

    def __repr__(self):
        return 'Dé à {0:4d} faces'.format(self.faces)

    def __iter__(self):
        while True:
            yield randint(1, self.faces+1)

lancer = iter(De(6))
# coups_de_des = [next(lancer)  for _ in range(1000)]
# %%
```

# Engendrer 1000 lancers de dés et calculer la fréquence de chaque face.

```python
# %%
d = {k:0  for k in range(1,7)}

for _ in range(1000):
    d[next(lancer)] += 1

for k, c in d.items():
    print('Face {0:2d} : {1:5d} occurrences,  fréquence {2:5.2f}%.'.format(k, c, c/1000.0))
# %%
```

### La solution `numpy`

> Un coup de dés, quand bien meme lancé des circonstances éternelles, jamais
n'abolira le hasard.


```python
# %%
d = {k:0  for k in range(1,7)}

def which(tab):
    # find the truthy !
    for i, x in enumerate(tab):
        if x:
            return i+1
    return None

for it in multinomial(1, np.ones(6)/6, 1000):
    d[which(it)] += 1

for k, c in d.items():
    print('Face {0:2d} : {1:5d} occurrences,  fréquence {2:5.2f}%.'.format(k, c, c/1000.0))

# %%
```


# Loi binomiale

## Triangle de Pascal

Écrire un programme qui, en fonction de $n$, renvoie les $n + 1$ premières lignes du triangle de Pascal.

```python
# %%
def triangle_Pascal(n=0):
    pascal = sc.zeros((n+1, n+1), dtype=int)
    pascal[:, 0] = 1  # un seul sous-ensemble vide dans chaque ensemble
    for i in range(1, n+1):
        pascal[i, 1:] = pascal[i-1, :-1] + pascal[i-1, 1:]
    return pascal

triangle_Pascal(5)
# %%
```

## Calcul de la fonction de masse et de la fonction de répartition de la loi binomiale

## Simulation de la loi binomiale par inversion de la fonction de répartition

## Simulation de la loi binomiale en utilisant la divisibilité de la loi binomiale


# Anniversaires

On considère un ensemble de $N$ personnes.
Pour simplifier le problème, on suppose que les anniversaires de ces $N$
personnes sont des variables indépendantes et identiquement distribuées, uniformes sur
$\{1, \ldots, 365\}.$ Écrire un programme qui, en fonction de $N$ et $k$,
simule une réalisation des $N$ dates
d’anniversaire, et renvoie si au moins $k$ personnes ont un anniversaire commun et $0$ sinon.

Meme question pour $P(E(3))$, la probabilité de l’événement
$$
\{\text{au moins trois des $N$ personnes ont un anniversaire commun}\}
$$


```python
# %%
from scipy.stats import randint
def anniversaires(N, k=2, p=365):
    dates = sorted(randint.rvs(low=0, high=p, size=N))
    runlength, val = 1, dates[0]
    for i in range(1, len(dates)):
        if (val==dates[i]):
            runlength += 1
            if runlength>=k:
                return True
        else:
            runlength, val = 1, dates[i]
    return False
# %%
```


# %%
from scipy.stats import randint
from collections import defaultdict

def anniversaires_(N, k=2, p=365):
    dates = defaultdict(lambda : 0)
    for d in randint.rvs(low=0, high=p, size=N):
        dates[d] += 1
    return k <= max(dates.values())
# %%
```


Ecrire alors un programme qui, en fonction de $N$ et $n$, simule $n$ réalisations indépendantes
des $N$ anniversaires, et renvoie la fréquence $q_N(n)$ de l’événement
$$
E(2) := \{\text{au moins deux des $N$ personnes ont un anniversaire commun}\}.
$$
À partir de quelle valeur de $N$ la probabilité $q_N := P(E(2))$ semble-t-elle dépasser $1/2$?
```python
# %%
n = 10000
sum([anniversaires_(23) for _ in range(n)])
# %%
```
Pour  $N = 22$
```python
# %%
int(sc.sqrt(365))+3
# %%
```
la probabilité de collision semble inférieure à $1/2$. Pour $N = 23$, la probabilité de collision
semble supérieure à $1/2$.

```python
# TODO: effectuer le calcul exact
```

Pour $N= 88$ la probabilité de collision semble supérieure à $1/2$.
```python
# %%
n = 10000
N = int(np.power(3*365**2,1/3))+15
sum([anniversaires(N, k=3) for _ in range(n)])
# %%
```

## Iterating over permutations


```python
# %%
# TODO: compute the determinant using Cramer's formula
# %%
```
