# %% markdown
# # Cahier de simulation pour Devoir 2021-22
#
# NOM : xxxx
# Prénom : xxxx
# %% codecell
# %% markdown
# Devoir II Controle continu 2020-2021
# Cours de Probabilités Master I MIDS Université de Paris
#
# %% codecell
import numpy as np
import pandas as pd    # >.25
import scipy.stats as ss
import cufflinks

import plotly.graph_objects as go  # > 4.8
import plotly.express as px

from time import time
from numba import jit
import numba
from tqdm import tqdm

# %% markdown
# Les `DataFrame` `pandas` sont équipés de méthodes graphiques. Ces méthodes font appel à un module de dessin qui peut être choisi. Par défaut, il s'agit de `matplotlib` mais on peut imposer `plotly` (au autre chose).
# %% codecell
pd.options.plotting.backend = "plotly"
# pd.options.plotting.backend
# %% markdown
# Initialisations. Fixer la graine du générateur aléatoire permet de reproduire les résultats.
# %% codecell
rng = np.random.default_rng(seed=42)


# %% codecell
%matplotlib inline
import matplotlib.pyplot as plt
# import numpy as np
# import pandas as pd
from dataclasses import dataclass
from scipy.stats import poisson
from collections import Counter
from IPython.core.display import HTML
# %% markdown
# Pour rendre les exécutions reproductibles, on fixe la graine du générateur aléatoire.
# %% codecell
# np.random.seed(4812)
# %% markdown
# Pour minimiser le code, on utilise le décorateur `dataclass` du paquetage [dataclasses](https://docs.python.org/3/library/dataclasses.html).
# Si `cite` est une instance de la classe `CRPgen`, l'état politique de la cité est représenté
# par les champs `cite.n` (nombre de citoyens courament dans la cité)  et `cite.profile` (profil de la partition courante, c'est un dictionnaire,
# l'entrée correspondant à la clé `r` indique le nombre de blocs/partis de taille `r` dans la partition courante.)
# Si `it` est un itérateur construit à partir de l'instance `cite`, une invocation de `next(it)` conduit à exécuter le code
# de la méthode `dunder` `__iter__`. À chaque invocation, on renvoie le profil de la partition courante (instruction `yield ...`).
# Et on insère un nouveau citoyen dans la partition courante. On met à jour le profil de la partition courante.
#
# Le profil de la partition courante est une instance de `Counter`, une variante des dictionnaires,
# adaptée au comptage. C'est une variation sur la classe `defaultdict`  du paquetage `collections`.
# %% codecell
@dataclass
class CRPgen():
    """Générateur pour le processus de fragmentation politique.

    Usage:
    ------

    g = CRPgen(theta=1)
    it = iter(g)
    next(it)
    """
    theta: float = .1  # parametre d'ambition
    n: int = 1  #nombre de citoyens

    def __post_init__(self):
        self.profile = Counter({1:1})


    def __iter__(self):
        while True:
          yield self.profile
          x = (self.n + self.theta) * np.random.random()
          for k, c in self.profile.items():
              x -= k*c
              if x < 0:   # insertion dans un parti de taille k
                  self.profile[k] -= 1
                  if self.profile[k]==0:
                    del self.profile[k]
                  self.profile[k+1] += 1
                  break
          else:  # création d'un nouveau parti
              self.profile[1] += 1
          self.n += 1  # mise à jour du nombre de citoyen

    def __repr__(self):
        return repr(self.profile)
# %% markdown
# On peut de cette façon réaliser le profil d'une cité aléatoire de $n=100 000$ citoyens.
# %% codecell
cite = CRPgen(theta=1)
it = iter(cite)
for _ in range(10000):
  next(it)  # à chaque itératition on met à jour le profile de la partition en insérant un nouveau citoyen dans le jeu politique

# cite.profile

# for r in sorted(cite.profile.keys()):
#     print(r, cite.profile[r])

u = pd.Series(cite.profile)\
      .sort_index()
df= pd.DataFrame(data={'Taille des partis': u.index, 'Nombre de partis': u.values})
display(HTML(df.to_html(index=False)))

# %% markdown
# ### Visualisations/approximations de la distribution du nombre de partis de taille $r$ dans une cité fragmenté
#
# On peut/veut se faire une idée de la distribution de $C_{n,r}$ pour différentes valeurs de $r= 1, \ldots$ et de $n$.
#
# La fonction `gen_histo_micros` renvoie un dictionnaire dont les clés correspondent aux $B$ réalisations observées de $C_{n,r}$, et
#      les valeurs au nombre de fois où on a observé un nombre de partis de taille $r$ correspondant à la clé
#      parmi les $B$ réalisations du processus de fragmentation.
#
# %% codecell
def gen_histo_micros(B=1000, theta=1, n=10000, r=1):
  """Engendre un échantillon de taille B, de réalisations indépendantes de C_{n, r}

     Renvoie un dictionnaire dont les clés correspondent aux réalisations observées de C_{n,r}, et
     les valeurs au nombre de fois où on a observé un nombre de partis de taille $r$ correspondant à la clé
     parmi les B réalisations du processus de fragmentation.
  """
  echant_nb_micros = Counter({0:0})
  for _ in range(B):
    cite = CRPgen(theta=theta)
    it = iter(cite)
    for _ in range(n):
      next(it)
    echant_nb_micros[cite.profile[r]] += 1
  return pd.Series(echant_nb_micros)
# %% markdown
# On veut illustrer le fait que lorsque $n \to \infty$, la loi du
# nombre de partis de taille $r$ dans la cité tend à ressembler à une loi de Poisson.
# Pour différentes valeurs de $n= 100, 1000, 10000$, on échantillonne le nombre de partis de taille $r$. L'échantillon
# est de taille $B$. Ici on s'intéresse aux micropartis ($r=1$).
# %% codecell
ns = [100, 500, 1000]  # différentes valeurs de $n$
theta = 10.
B = 1000
r = 1
histos = [gen_histo_micros(B=B, theta=theta, n=m, r=r) for m in ns]  # préparation des données pour graphique
# 5.63 s ± 246 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)

labels = histos[0].index
for i in range(1, len(ns)):
  labels = labels.union(histos[i].index)   # préparation du graphique

x = np.array(labels, dtype=int) # préparation du graphique
width = 0.6/(1+len(ns))         # préparation du graphique
# %% markdown
# On visualise la distribution empirique du nombre de partis de taille $r$
# en traçant la densité (par rapport à la mesure comptage, c'est donc la _probability mass function_).
# %% codecell
fig, ax = plt.subplots()

for i in range(len(ns)):
  ax.bar(histos[i].index + (i-(len(ns)+1)/2)*width,
         histos[i].values/B,
         width=width,
         align='edge',
         label='n= {0}'.format(ns[i]))

ax.bar(x + (len(ns)-(len(ns)+1)/2)*width,
       poisson.pmf(x, theta/r),
       width=width,
       align='edge',
       label='Poisson({0})'.format(theta/r))

#
ax.set_ylabel('Proportion among {0} experiments'.format(B))
ax.set_xlabel('Number of parties of size {0}'.format(r))
ax.set_title('Political fragmentations for different n')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()


fig.tight_layout()

plt.show()
# %% markdown
# On veut maintenant décrire approximativement la distribution de $|B_n|$ la manière dont cette distribution dépend de $\theta$.
#
# Pour différentes valeurs de $\theta$ ($1, 2, 10, 100$), pour $n=1000, 10000, 100000$, simuler $B=10000$ tirages de $|B_n|$ et tracer les diagrammes en batons (barplots) correspondants aux différentes choix des paramètres $\theta$ et $n$.
# %% codecell
def mean_Bn(n=1000, theta=1.0):
    return theta * np.sum(1/(theta+ np.arange(1, n+1)))

# %% codecell
# À Vous !
def gen_histo_nb_partis_lent(B=1000, theta=1, n=10000):
  """Engendre un échantillon de taille B, de réalisations indépendantes de |B_n|

     Renvoie un dictionnaire dont les clés correspondent aux réalisations observées de |B_n|, et
     les valeurs au nombre de fois où on a observé un nombre de partis correspondant à la clé
     parmi les B réalisations du processus de fragmentation.
  """
  echant_nb_partis = Counter({0:0})
  for _ in range(B):
    cite = CRPgen(theta=theta)
    it = iter(cite)
    for _ in range(n):
      next(it)
    echant_nb_partis[sum(cite.profile.values())] += 1
  return pd.Series(echant_nb_partis)
# %% codecell
ns = [1000,]  # différentes valeurs de $n$
theta = 10.
B = 10000
histos = [gen_histo_nb_partis_vite(B=B, theta=theta, n=m) for m in ns]  # préparation des données pour graphique


labels = histos[0].index
for i in range(1, len(ns)):
  labels = labels.union(histos[i].index)   # préparation du graphique

x = np.array(labels, dtype=int) # préparation du graphique
width = 0.6/(1+len(ns))         # préparation du graphique
# %% markdown
# On visualise la distribution empirique du nombre de partis de taille $r$
# en traçant la densité (par rapport à la mesure comptage, c'est donc la _probability mass function_).
# %% codecell
fig, ax = plt.subplots()

for i in range(len(ns)):
  ax.bar(histos[i].index + (i-(len(ns)+1)/2)*width,
         histos[i].values/B,
         width=width,
         align='edge',
         label='n= {0}'.format(ns[i]))

ax.bar(x + (len(ns)-(len(ns)+1)/2)*width,
       poisson.pmf(x, theta/r),
       width=width,
       align='edge',
       label='Poisson({0})'.format(theta/r))

#
ax.set_ylabel('Proportion among {0} experiments'.format(B))
ax.set_xlabel('Number of parties of size {0}'.format(r))
ax.set_title('Political fragmentations for different n')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()


fig.tight_layout()

plt.show()

# %% codecell
n = 1000
theta = 10

# %% codecell

def gen_histo_nb_partis_vite(B=1000, theta=1, n=10000):
  """Engendre un échantillon de taille B, de réalisations indépendantes de |B_n|

     Renvoie un dictionnaire dont les clés correspondent aux réalisations observées de |B_n|, et
     les valeurs au nombre de fois où on a observé un nombre de partis correspondant à la clé
     parmi les B réalisations du processus de fragmentation.
  """
  etalon = theta + np.arange(1, n+1)
  s = pd.Series([sum(np.random.random(n) * etalon < theta) for _ in range(B)])
  return s.value_counts().sort_index()
# %% codecell

def gen_histo_nb_partis_vite_2(B=1000, theta=1, n=10000):
  """Engendre un échantillon de taille B, de réalisations indépendantes de |B_n|

     Renvoie un dictionnaire dont les clés correspondent aux réalisations observées de |B_n|, et
     les valeurs au nombre de fois où on a observé un nombre de partis correspondant à la clé
     parmi les B réalisations du processus de fragmentation.
  """
  etalon = theta + np.arange(1, n+1)
  s = Counter(sum(np.random.random(n) * etalon < theta) for _ in range(B))
  return s

# %% codecell
def gen_histo_nb_partis_vite_3(B=1000, theta=1, n=10000):
  """Engendre un échantillon de taille B, de réalisations indépendantes de |B_n|

     Renvoie un dictionnaire (Counter) dont les clés correspondent aux réalisations observées de |B_n|, et
     les valeurs au nombre de fois où on a observé un nombre de partis correspondant à la clé
     parmi les B réalisations du processus de fragmentation.
  """
  etalon = list(theta + np.arange(1, n+1))
  s = Counter(sum(1 for x, e in zip(np.random.random(n), etalon)
                    if (x * e < theta ))
              for _ in tqdm(range(B))
              )
  return s

# %% codecell
# %% codecell
ns = [100, 500, 1000, 5000, 10000, 20000, 50000, 100000]  # différentes valeurs de $n$
theta = 0.1
B = 10000
# %% codecell
%timeit

histos = [gen_histo_nb_partis_vite_3(B=B, theta=theta, n=m) for m in ns]  # préparation des données pour graphique

# 202 ms ± 1.71 ms per loop (mean ± std. dev. of 7 runs, 1 loop each) etalon is a list
# 239 ms ± 2.66 ms per loop (mean ± std. dev. of 7 runs, 1 loop each) etalon is a ndarray  n=1000, B=1000
# 100%|██████████| 10000/10000 [04:14<00:00, 39.29it/s] n =100000, B =10000
# trading memory for time we could replace the ndarray etalon by a generator randge(1, n+1)
# %% codecell
# %%timeit
# histos = [gen_histo_nb_partis_vite_2(B=B, theta=theta, n=m) for m in ns]  # préparation des données pour graphique
# 2.4 s ± 4.09 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
# %% codecell
# %%timeit
# histos = [gen_histo_nb_partis_vite(B=B, theta=theta, n=m) for m in ns]  # préparation des données pour graphique
# 2.4 s ± 5.79 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)
# %% codecell
mus = [mean_Bn(n, theta) for n in ns] # theta * np.sum(1/(theta+ np.arange(1, ns[0]+1)))
histos = [pd.Series(h).sort_index() for h in histos]


# %% codecell
labels = histos[0].index
x = np.array(labels, dtype=int) # préparation du graphique
width = 0.6/(1+1)               # préparation du graphique
# %% markdown
# On visualise la distribution empirique du nombre de partis dans une cité de taille $n$
# en traçant la densité (par rapport à la mesure comptage, c'est donc la _probability mass function_).
# %% codecell

# %% codecell
fig, ax = plt.subplots()

for i in range(len(ns)):
  ax.bar(histos[i].index + (i-(len(ns)+1)/2)*width,
         histos[i].values/B,
         width=width,
         align='edge',
         label='n= {0}'.format(ns[i]))

ax.bar(x + (len(ns)-(len(ns)+1)/2)*width,
       poisson.pmf(x, mu),
       width=width,
       align='edge',
       label=f'Poisson({mu:.2f})')

#
ax.set_ylabel(f'Proportion among {B} experiments')
ax.set_xlabel('Number of parties')
ax.set_title(f'Number of political parties for n = {ns[0]} and theta = {theta:.2f}')
ax.set_xticks(x[:-1:5])
ax.set_xticklabels(labels[:-1:5])
ax.legend()

plt.plot()
plt.xticks(rotation = 45)

fig.tight_layout()
# fig.savefig('|B_n|_n=100000_theta=10.png', format='png')
plt.show()
# plt.savefig('|B_n|_n=100000_theta=10.png', format='png')

# %% codecell
def dist2poisson(pmf_empirique, n, B, theta):
    mu = mean_Bn(n, theta)
    dtv = 0.
    for k in np.arange(np.max(pmf_empirique.index)+1):
        if k in pmf_empirique.index:
            dtv += np.abs(pmf_empirique[k]/B - poisson.pmf(k, mu))
        else:
            dtv += poisson.pmf(k, mu)
    dtv += 1 - poisson.cdf(np.max(pmf_empirique.index), mu)
    return dtv


[dist2poisson(h, n, B, theta)  for h, n in zip(histos, ns)]

mean_Bn(100000, theta)
theta * np.log(1 + 100000/theta)
# Distance en variation de l'approximation empirique de la loi de Bn à la Poisson de meme espérance
# pour n
# [0.22124118111003074,
#  0.14488761208458442,
#  0.11719609754063304,
#  0.09507093058069069,
#  0.0977568497461772,
#  0.06730174385380176,
#  0.08424006515422446,
#  0.0843739135251462]

# %% markdown
## TODO
- [ ] plotly graphics
- [x] make jit work
- [x] add progress bar
