# -*- coding: utf-8 -*-
# %% markdown [markdown]
# Devoir II Controle continu 2020-2021
# Cours de Probabilités Master I MIDS Université de Paris
#
# %% codecell
import numpy as np
import pandas as pd    # >.25
import scipy.stats as ss
import cufflinks

import plotly.graph_objects as go  # > 4.8
import plotly.express as px

from time import time
from numba import jit
import numba
from tqdm import tqdm

# %% [markdown]
# Les `DataFrame` `pandas` sont équipés de méthodes graphiques. Ces méthodes font appel à un module de dessin qui peut être choisi. Par défaut, il s'agit de `matplotlib` mais on peut imposer `plotly` (au autre chose). 

# %% codecell
pd.options.plotting.backend = "plotly"
# pd.options.plotting.backend 

# %% [markdown]
# Initialisations. Fixer la graine du générateur aléatoire permet de reproduire les résultats.  

# %% codecell
rng = np.random.default_rng(seed=42)

h = .3    # dérive de la marche aléatoire 2 * h
n = 10000  # longueur de la trajectoire
B = 10000  # nombre de répliques de la trajectoire
a = 5000   # seuil à franchir

width = 8        # f-string
precision = 2    # f-string

# %% markdown [markdown]
# Engendrer `B` trajectoires de longueur `n`, de dérive $Δ = $ `2 * h`. Le résultat
# est un `ndarray` à `n` lignes et `B` colonnes. Chaque colonne représente
# une trajectoire de la marche alétoire. 
#
#
# %% codecell
paths = rng.choice(np.array([-1, 1]),
                   size=(n, B),
                   p=.5 + h * np.array([-1, 1]),
                   shuffle=False)\
           .cumsum(axis=0)

# %% markdown [markdown]
# Définir une fonction `gen_rw` dont la spécification est décrite dans le `docstring`.
#
# On essaye de suivre l'API de `numpy.random`. Les premiers arguments de la fonction sont des paramètres de la distribution à simuler. L'argument `size` prend en argument un tableau 
# qui prescrit la forme (`shape`) du résultat. Le résultat sera peuplé de réalisations (réputées i.i.d.) de la distribution paramétrée.  
#
# En utilisant la méthode `cumsum`  des tableaux `numpy`, on évite les boucles `for`, on gagne en efficacité. 
# %% codecell
def gen_rw(drift=.5, size=(1000, 1000), rng=np.random.default_rng(seed=42)):
    """Generate `size[1]` random walks of lenth `size[0]` with `drift`.

    Parameters
    ----------
    drift : float
        $\mathbb{E}[X_i] = \text{drift}$
    size : tuple of int
        (n, B) B random walks .

    Returns
    -------
    ndarray with shape size and dtype float
        The random walks for further use.

    """
    paths = rng.choice(np.array([-1, 1]),
                       size=size,
                       p=.5 * (1. + drift * np.array([-1, 1])),
                       shuffle=False)\
               .cumsum(axis=0)
    return paths

# %% markdown [markdown]
# Definir une fonction `hitting_time` qui prend en argument un tableau `ndarray`
# qui représente une trajectoire de la marche aléatoire (jusqu'à l'instant correspondant
#  à la longueur du tableau) et un argument optionnel
#  `threshold` et renvoie le premier instant (indice) auquel la trajectoire atteint
# la valeur de `threshold`.
#
# Traiter soigneusement le cas où le seuil n'est pas atteint: quel genre de résultat renvoyer?
# Essayer d'anticiper la possibilité de calculer des statistiques avec `np.mean`, `np.namean`, ....
#
# On ne cherche pas ici à éviter la boucle `for`, mais on utilise le compilateur `jit` de `numba` pour obtenir des temps d'exécution décents. 
#
# Noter l'usage de la clause `else` de la boucle `for`. 
# %% codecell
@jit # get it faster using numba.jit
def hitting_time(path, threshold=100):
    """Short summary.

    Parameters
    ----------
    path : type
        Description of parameter `path`.
    threshold : type
        Description of parameter `threshold`.

    Returns
    -------
    type
        Description of returned object.

    """
    for i, y in enumerate(path):
        if y == threshold:
            return np.float(i)
    else:
        return np.nan  # no trespassing

# %% markdown [markdown]
# Simuler  $1000$ temps d'atteinte du seuil `5000` par une  marche aléatoire de dérive $\Delta = .6$.
# %% codecell
spam = gen_rw(.6, (10000, 1000))
before = time()
np.apply_along_axis(hitting_time,
                    0,
                    spam,
                    threshold=np.int(5000))
duration = time() - before
print(f'Elapsed time: {duration:5.3} seconds')
# %% markdown [markdown]
# Calculer l'espérance et la variance empiriques de votre échantillon de temps d'atteinte.
# %% codecell
threshold = 5000
hit_times = np.apply_along_axis(hitting_time,
                                0,
                                paths, ** {'threshold': threshold})

print('Mean hitting time: {0:5.3f}, Standard deviation: {1:5.3f}'\
                .format(np.nanmean(hit_times),
                        np.nanstd(hit_times)))
# %% markdown [markdown]
# Dessiner dix trajectoires de marches aléatoires de dérive $\Delta$ jusqu'à l'instant $n=10000$.
#
# Dessiner une droite horizontale à l'ordonné correspondant à la valeur de `threshold`.
# %% codecell
h, n, B, threshold = .3, 10000, 10, 5000
drift = 2*h
paths = gen_rw(2*h, (n, B))

paths.shape
hit_times = np.apply_along_axis(hitting_time,
                                0,
                                paths, ** {'threshold': threshold})
# %% codecell
fig = go.Figure()

for c in range(min(paths.shape[1], 10)):
    fig.add_trace(go.Scatter(x=np.arange(paths.shape[0]), y=paths[:, c], mode='lines'))

fig.add_trace(go.Scatter(x=np.arange(paths.shape[0]),
                         y=2 * h * np.arange(paths.shape[0]),
                         mode='lines',
                         line = dict(color='royalblue', width=1, dash='dash'),
                         name = f'y = {2*h}* x'))
fig.update_layout(
    title_text=f'Biased random walks, drift: {2*h} ', # title of plot
    xaxis_title_text='n', # xaxis label
    yaxis_title_text='Sn' #, # yaxis label
)
fig.show()

# %% markdown [markdown]
# On ajoute deux lignes pointillées pour marquer le seuil (`thershold`) et le temps moyen auquel ce seuil est franchi par les trajectoires aléatoires. 
#
# On peut zoomer autour de l'intersection des deux lignes pour observer que les trajectoires aléatoires oscillent autour de la droite de pente `drift`. 

# %% codecell

fig.add_trace(go.Scatter(x=np.arange(paths.shape[0]),
                         y=threshold * np.ones(paths.shape[0]),
                         mode='lines',
                         line = dict(color='red', width=1, dash='dot'),
                         name = None))

fig.add_trace(go.Scatter(x=np.full(paths.shape[0], threshold/drift),
                         y=np.arange(paths.shape[0]), 
                         mode='lines',
                         line = dict(color='red', width=1, dash='dot'),
                         name = None))

fig.show()
# %% [markdown]
# On engendre maintenant un échantillon assez grand de trajectoires pour pouvoir faire des statistiques sur les temps d'atteinte du seuil. 
#
# Choisir `B` en fonction des possibilités de la machine 

# %%
h, n, B, threshold = .3, 10000, 10000, 5000
drift = 2*h
paths = gen_rw(2*h, (n, B))

paths.shape
hit_times = np.apply_along_axis(hitting_time,
                                0,
                                paths, ** {'threshold': threshold})

# %% markdown [markdown]
# Tracer un croquis de l'histogramme de l'échantillon des temps d'atteinte
# du seuil $5000$ pour une marche aléatoire
# de dérive $0.6$. Vous superposerez à l'histogramme la densité gaussienne dont l'espérance
# et la variance sont ajustées à la moyenne empirique et à la variance empirique de l'échantillon.
#
# Vous trouverez dans `scipy.stats` les fonctions qui permettent de tracer la densité,
# la fonction de répartition, etc des gaussiennes et de bien d'autres lois classiques.

# %% codecell
fig = go.Figure()

range_hit_times = map(lambda f : f(hit_times), [np.min, np.max])
grid_times = np.linspace(* range_hit_times, 100)

fig = go.Figure()

fig.add_trace(go.Histogram(x=hit_times,
                           histnorm='probability density',
                           name='Hitting times'))

fig.add_trace(go.Scatter(x=grid_times,
                         y=ss.norm.pdf(grid_times,
                                       loc=np.nanmean(hit_times),
                                       scale=np.nanstd(hit_times)),
                         mode='lines',
                         line=dict(color='red', width=2, dash='dot'),
                         name='Fitted normal density'))

fig.add_trace(go.Scatter(x=grid_times,
                         y=ss.norm.pdf(grid_times,
                                       loc=threshold/drift,
                                       scale=np.sqrt((threshold/(drift**3)) * (1-drift**2))),
                         mode='lines',
                         line=dict(color='blue', width=2, dash='dash'),
                         name='Theoretical normal density'))
fig.update_layout(
    title_text=(f'Hitting times for biased random walks drift: {2*h}, '
                '\n'
                f'threshold: {threshold}, Expected hitting time: {threshold/(2*h)}:{width}.{precision} '), # title of plot
    xaxis_title_text='Hitting times', # xaxis label
    yaxis_title_text='Demsity' #, # yaxis label
)
fig.show()


# %% markdown [markdown]
# Construire un diagramme de dispersion (nuage de points, scatterplot)
# dont les points correspondent aux trajectoires: en abscisse on choisira
# l'écart entre la trajectoire et son espérance à l'instant $\text{threshold}/\text{drift}$,
# en ordonnée, on choisira l'écart entre le temps d'atteinte du seuil
# et $\text{threshold}/\text{drift}$.

# %% codecell
def triangle(path, drift, threshold=100):
    """Short summary.

    Parameters
    ----------
    path : type
        Description of parameter `path`.
    drift : type
        Description of parameter `drift`.
    threshold : type
        Description of parameter `threshold`.

    Returns
    -------
    type
        Description of returned object.

    """
    ht = hitting_time(path, threshold)
    eht = int(np.floor(threshold/drift))
    return np.array((path[eht]-threshold, eht-ht))

#triangle(paths[:,0], .6, 5000)
# %% codecell
foo = np.apply_along_axis(triangle,
                    0,
                    paths,
                    ** {'threshold': threshold, 'drift': .6})\
        .transpose()
foo.shape
df = pd.DataFrame(foo, columns=['Deviation at EHT', 'Recentered HT'])
# %% [markdown]
# We can use the `plotly`  backend for `pandas` to perform quick plots

# %%
fig = df.plot.scatter(x='Deviation at EHT', 
                y='Recentered HT',     
                opacity=.1)

# tune to simulation size 
fig.add_trace(go.Scatter(x=np.linspace(-500, 500),
                         y=np.linspace(-500, 500)/drift,
                         line=dict(color='firebrick',
                                        dash='dot'),
                         name=None))

# 
fig.update_layout(
        title_text=(f'Deviation of hitting time with respect to deviation at expected hitting time for biased random walks drift: {2*h}, '
                '\n'
                f'threshold: {threshold}, Expected hitting time: {threshold/(2*h)}:{width}.{precision} '), # title of plot
        xaxis_title_text='Deviation of random walk at expected hitting time', # xaxis label
        yaxis_title_text='Hitting time - expected hitting time' #, # yaxis label
    )

fig.show()

# %% markdown [markdown]
# Définir une fonction qui prend en argument une dérive (drift)  et
# un seuil (threshold) qui renvoie un générateur de temps d'atteinte
# du seuil par une marche aléatoire avec dérive.

# %% markdown


# %% codecell
fig = px.scatter(df,
                 x= 'Deviation at EHT',
                 y= 'Recentered HT',
                 opacity=.25)


fig.update_layout(
    title_text=(f'Deviation of hitting time with respect to deviation at expected hitting time for biased random walks drift: {2*h}, '
                '\n'
                f'threshold: {threshold}, Expected hitting time: {threshold/(2*h)}:{width}.{precision} '), # title of plot
    xaxis_title_text='Deviation of random walk at expected hitting time', # xaxis label
    yaxis_title_text='Hitting time - expected hitting time' #, # yaxis label
)
fig.show()

# %% codecell
drift = 2 * h

fig =  go.Figure()

fig.add_trace(go.Scatter(x=df['Deviation at EHT'],
                         y=drift * df['Recentered HT'],
                         mode='markers',
                         marker_size=6,
                         opacity=.25
                         ))

# tune to simulation size
fig.add_trace(go.Scatter(x=np.linspace(-500, 500),
                         y=np.linspace(-500, 500),
                         line=dict(color='firebrick',
                                        dash='dot'),
                         name=None))

fig.update_layout(
    title_text=(f'Deviation of hitting time with respect to deviation at expected hitting time for biased random walks drift: {2*h}, '
                '\n'
                f'threshold: {threshold}, Expected hitting time: {threshold/(2*h)}:{width}.{precision} '), # title of plot
    xaxis_title_text='Deviation of random walk at expected hitting time', # xaxis label
    yaxis_title_text='Rescaled hitting time - expected hitting time' #, # yaxis label
)
fig.show()


# %% [markdown]
# Ce que ces dessins suggèrent, c'est une corrélation très forte entre l'écart entre le temps d'atteinte du seuil (_Hitting Time - Expected Hitting Time_) et l'écart entre la marche aléatoire et son espérance à l'instant _Expected Hitting Time_. 
#
# Cette observation n'est pas fortuite. En travaillant un peu plus, on peut montrer que 
# les deux quantités sont liées à une perturbation aléatoire près qu'on appelle ici un résidu. 
#
# Ce résidu est typiquement petit devant le l'écart entre le temps d'atteinte du seuil (_Hitting Time - Expected Hitting Time_) et l'écart entre la marche aléatoire et son espérance à l'instant _Expected Hitting Time_. Ces deux écarts sont typiquement de l'ordre de la racine carré du _Expected Hitting Time_. En démontrant cela, on établit un théorème central limite 
# pour _Hitting Time - Expected Hitting Time_ lorsque le seuil `threshold` devient grand. 
#
# Nous traçons ici l'histogramme des résidus normalisés. 

# %% codecell

fig =  go.Figure()

residuals =  drift * df['Recentered HT'] - df['Deviation at EHT']

fig.add_trace(go.Histogram(x=residuals/np.std(residuals),
                         histnorm='probability density',
                         nbinsx=70)
              )

fig.add_trace(go.Scatter(x=np.linspace(-2, 2, 100),
                         y=ss.norm.pdf(np.linspace(-2, 2, 100)),
                         mode='lines',
                         line=dict(color='red', width=2, dash='dot'),
                         name='Normal density'))

fig.update_layout(
    title_text=f'Standardized residuals',
    xaxis_title_text='Residuals', # xaxis label
    yaxis_title_text='Density' #, # yaxis label
)
fig.show()

# %% codecell
