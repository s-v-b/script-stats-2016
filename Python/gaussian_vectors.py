# -*- coding: utf-8 -*-
# %% markdown

# Devoir III 2020-21 : Vecteurs gaussiens


# %% codecell

import numpy as np
import scipy.stats as ss

from numba import jit
from tqdm import tqdm

from numpy.linalg import cholesky
from numpy.linalg import svd
from scipy.stats import multivariate_normal

import pandas as pd

import plotly.express as xp
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from datetime import datetime

# datetime.now().strftime("%dst %B %Y")
# datetime.now().date().strftime("%A %d %B %Y")
# %% codecell
rng = np.random.default_rng(seed=42)

# %% codecell
n, d, B = 1000, 3, 10000

xx = rng.normal(size=(n, d))


# %% markdown

## Graphiques de distributions

- Tracer les densités de $\mathcal{N}(\mu, \sigma^2)$ pour $\mu \in \{0,1\}$ et
$\sigma \in \{1, 2, 1/2\}$

- Tracer les fonctions de répartition de $\mathcal{N}(\mu, \sigma^2)$ pour $\mu \in \{0,1\}$ et
$\sigma \in \{1, 2, 1/2\}$

- Tracer les fonctions quantiles de $\mathcal{N}(\mu, \sigma^2)$ pour $\mu \in \{0,1\}$ et
$\sigma \in \{1, 2, 1/2\}$

# %% markdown
[Scipy playing with Gaussian distributions](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.norm.html)
# %% codecell
# fig = go.Figure()
mus, sigmas = np.array([0, 1, 0, 2]), np.array([1, 2, 1/2, 1/4])
texts = [f'μ: {mu}   σ: {sig}' for mu, sig in zip(mus, sigmas)]
colors = ["Blue", "Red", "Green", "Purple"]

x = np.linspace(-6, 6, num=1000)

fig = make_subplots(rows=1,
                    cols=2,
                    subplot_titles=("Densities", "CDF")
                    )

for i, params in enumerate(zip(mus, sigmas)):
    mu, sig = params
    fig.append_trace(
        go.Scatter(x=x,
                   y=ss.norm.pdf(x=x, **{'loc':mu, 'scale':sig}),
                   name=texts[i],
                   legendgroup=texts[i],
                   mode='lines',
                   line=dict(color=colors[i]),
                   text=texts[i]),
        row=1,
        col=1
    )
    fig.append_trace(
        go.Scatter(x=x,
                   y=ss.norm.cdf(x=x, **{'loc':mu, 'scale':sig}),
                   mode='lines',
                   legendgroup=texts[i],
                   showlegend= False,
                   line=dict(color=colors[i]),
        ),
        row=1,
        col=2,
    )

fig.update_layout(
    dict(
         title= 'Gaussian distributions')
)

fig.show()
# %% markdown

- Tracer les densités de $\mathcal{N}(\mu, \Sigma)$ pour $\mu=\left(\begin{array}{c} 0 \\ 0 \end{array}\right)$ et
$\Sigma = \begin{pmatrix} 2 & 1 \\ 1 & 1 \end{pmatrix}$. Vous tracerez les contours correspondant

 (_suggestion_: ...)

- Tracer les ellipses de centre $\mu$, d'axes définis par les vecteurs propres de $\Sigma$,
de ...

- Tracer les niveaux de la
# %% codecell


# %% codecell
Sigma = np.array([[2, 1], [1, 1]])
Precision = np.linalg.solve(Sigma, np.eye(2))

# %% markdown
Making contourplot is just like plotting a function over $\mathbb{R}$.
Rather than taking a grid on a segment, we take grid on a rectangle.
To build a grid on a segment,  we may use `linspace`. To build a grid on a rectangle, we
may call `meshgrid` or `mgrid`.

[`meshgrid` versus `mgrid`](http://louistiao.me/posts/numpy-mgrid-vs-meshgrid/)
# %% codecell
# make a grid
# x, y = np.meshgrid(np.linspace(-3, 3), np.linspace(-3,3))

def foo(x, y):
    global Sigma
    pass

# %% codecell

rv = multivariate_normal([0, 0], Sigma)
x, y = np.mgrid[-2:2:.01, -2:2:.01]
# pos = np.dstack((x, y))
z = rv.pdf(np.dstack(np.mgrid[-2:2:.01, -2:2:.01]))


fig = go.Figure(data =
    go.Contour(
        z= z,
        x= x[:,0],
        y= y[0,:]
    )
)

fig.add_trace(

)
fig.update_layout(
    dict(
         title= 'Gaussian density in dimension 2')
)
fig.show()
# %% codecell
# evaluate Gaussian density on the grid
# %% codecell build a contour plot

# np.linalg.eig(Sigma)

# %% markdown
## Ceci n'est pas un vecteur gaussien


# %% codecell
rr = rng.choice(np.array([-1, 1], dtype=np.int), size=(n, d))
yy = xx * rr
yy.shape


# %% markdown
## Méthode de Box et Müller

# %% codecell
xx.shape

# %% markdown
## Simulation de vecteurs gaussiens de covariance donnée



# %% codecell
d = 2
A =  rng.normal(size=(d, d))
Sig = A.transpose() @ A
np.linalg.eig(Sig)
L =  cholesky(Sig)
np.linalg.eigvals(Sig)

s = L @ rng.normal(size=(d, n))
mun = np.mean(s, axis=1)
Sign = np.cov(s)
mun.shape

# %% codecell


# %% markdown

## Normes de vecteur gaussien standards


- Tracer la densité de la loi de la norme d'un vecteur gaussien standard de
dimension $d$ pour $d \in \{10, 20, 50, 100\}$

- Tracer la fonction quantile de la loi de la norme d'un vecteur gaussien standard de
dimension $d$ pour $d \in \{10, 20, 50, 100\}$

- Calculer l'écart interquartile de la loi de la norme d'un vecteur gaussien standard de
dimension $d$ pour $d \in \{1,\ldots, 1000\}$. Tracer la courbe correspondante.

# %% markdown

## Normes de vecteurs gaussiens centrés

# %% codecell

# %% markdown

## Théorème de Cochran

- Projecteurs et décomposition spectrale
- Projection d'un vecteur gaussien standard sur des sous-espaces orthogonaux.


# %% markdown

## Théorème de Student

- Indépendance moyenne et variance empirique


# %% markdown
## Maxima de gaussiennes indépendantes

- Engendrer le maximum de $n$ gaussiennes centrées réduites  indépendantes ($n=1e5$).
- Tracer l'histogramme
- Standardiser l'échantillon en le recentrant autour $\Phi^{←}(1-1/n)$
et en multipliant l'échantillon recentré par $\sqrt{2\log n}$.
- Tracer la fonction de répartition empirique de l'échantillon recentré
et standardisé.
- Superposer la fonction de répartition de la [loi de Gumbel](https://docs.scipy.org/doc/scipy/reference/tutorial/stats/continuous_gumbel_r.html)

# %% codecell
ss.gumbel_l

# %% markdown
## Maxima de gaussiennes corrélées

$$\Sigma = \begin{pmatrix}1 & ρ & \ldots & ρ \\
                \rho & 1 &  ⋱    & ρ \\
                ⋮    & ⋱ &  ⋱    & ⋱\\
                ρ    & … & ρ & 1 \end{pmatrix}$$

Simuler un $n$-échantillon de vecteurs gaussiens centrés de covariance $\Sigma$.

# %% codecell
