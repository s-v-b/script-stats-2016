# -*- coding: utf-8 -*-
# %% markdown [markdown]
# Devoir II Controle continu 2020-2021
# Cours de Probabilités Master I MIDS Université de Paris
#
# %% codecell
import numpy as np
import pandas as pd
import scipy.stats as ss

import plotly.graph_objects as go
import plotly.express as px

from time import time
from numba import jit
import numba
from tqdm import tqdm
# %% codecell
rng = np.random.default_rng(seed=42)

h = .3    # dérive de la marche aléatoire 2 * h
n = 10000  # longueur de la trajectoire
B = 10000  # nombre de répliques de la trajectoire
a = 5000   # seuil à franchir

width = 8        # f-string
precision = 2    # f-string


# %% markdown [markdown]
# Engendrer `B` trajectoires de longueur `n`, de dérive $Δ = $ `2 * h`. Le résultat
# est un `ndarray` à `n` lignes et `B` colonnes. Chaque colonne représente
# une trajectoire de la marche alétoire
# %% codecell
# paths = 

# %% markdown [markdown]
# Définir une fonction `gen_rw` dont la spécification est décrite dans le `docstring`
# %% codecell
def gen_rw(drift=.5, size=(1000, 1000), rng=np.random.default_rng(seed=42)):
    """Generate `size[1]` random walks of lenth `size[0]` with `drift`.

    Parameters
    ----------
    drift : float
        $\mathbb{E}[X_i] = \text{drift}$
    size : tuple of int
        (n, B) B random walks .

    Returns
    -------
    ndarray with shape size and dtype float
        The random walks for further use.

    """
    pass

# %% markdown [markdown]
# Definir une fonction `hitting_time` qui prend en argument un tableau `ndarray`
# qui représente une trajectoire de la marche aléatoire (jusqu'à l'instant correspondant
#  à la longueur du tableau) et un argument optionnel
#  `threshold` et renvoie le premier instant (indice) auquel la trajectoire atteint
# la valeur de `threshold`.
# Traiter soigneusement le cas où le seuil n'est pas atteint: quel genre de résultat renvoyer?
# Essayer d'anticiper la possibilité de calculer des statistiques avec `np.mean`, `np.namean`, ....
#
# %% codecell
# @jit # get it faster using numba.jit
def hitting_time(path, threshold=100):
    """Short summary.

    Parameters
    ----------
    path : type
        Description of parameter `path`.
    threshold : type
        Description of parameter `threshold`.

    Returns
    -------
    type
        Description of returned object.

    """
    pass

# %% markdown [markdown]
# Simuler  $1000$ temps d'atteinte du seuil `5000` par une  marche aléatoire de dérive $\Delta = .6$.
# %% codecell
before = time()
# TODO:
# do something 
duration = time() - before
print(f'Elapsed time: {duration:5.3} seconds')
# %% markdown [markdown]
# Calculer l'espérance et la variance empiriques de votre échantillon de temps d'atteinte.
# %% codecell
# TODO:
# %% markdown [markdown]
# Dessiner dix trajectoires de marches aléatoires de dérive $\Delta$ jusqu'à l'instant $n=10000$.
#
# Dessiner une droite horizontale à l'ordonné correspondant à la valeur de `threshold`.
# %% codecell
# TODO:
# %% codecell
fig = go.Figure()

# TODO:

fig.show()

# %% markdown [markdown]
# Tracer un croquis de l'histogramme de l'échantillon des temps d'atteinte
# du seuil $5000$ pour une marche aléatoire
# de dérive $0.6$. Vous superposerez à l'histogramme la densité gaussienne dont l'espérance
# et la variance sont ajustées à la moyenne empirique et à la variance empirique de l'échantillon.
#
# Vous trouverez dans `scipy.stats` les fonctions qui permettent de tracer la densité,
# la fonction de répartition, etc des gaussiennes et de bien d'autres lois classiques.

# %% codecell
fig = go.Figure()

# TODO:

fig.show()


# %% markdown [markdown]
# Construire un diagramme de dispersion (nuage de points, scatterplot)
# dont les points correspondent aux trajectoires: en abscisse on choisira
# l'écart entre la trajectoire et son espérance à l'instant $\text{threshold}/\text{drift}$,
# en ordonnée, on choisira l'écart entre le temps d'atteinte du seuil
# et $\text{threshold}/\text{drift}$.

# %% codecell
def triangle(path, drift, threshold=100):
    """Short summary.

    Parameters
    ----------
    path : type
        Description of parameter `path`.
    drift : type
        Description of parameter `drift`.
    threshold : type
        Description of parameter `threshold`.

    Returns
    -------
    type
        Description of returned object.

    """
    pass
# %% codecell
# TODO:
# %% codecell
