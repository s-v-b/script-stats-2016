# SCRIPTS illustrant le cours de statistique 2017-18 #


### Objectif ? ###

* Notebooks python et R  destinés à illustrer le [cours de statistique du Magistère FIMFA 2017-18](http://stephane-v-boucheron.fr/teaching/statistiques/)
* Version 0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Usage ? ###

* installer Python 3 sur votre machine
* Installer **Jupyter**  (`pip3 install jupyter`)
* télécharger les notebooks qui vous intéressent
* lancer `jupyter notebook nb.ipynb` où  `nb`  est le nom d'un notebook qui vous intéresse.  


* installer Rstudio et tous les packages fournis par Rstudio
* charger les fichiers `*.Rmd`du répertoire `R` (Rmarkdown)
* consulter *Advanced R programming* de H. Wickham
* concevez vos graphiques avec `ggplot2`
* restez lisible grâce à `%>%` (`magrittr`)

* En Python `pweave` essaye d'apporter les facilités offertes par RMarkdown

