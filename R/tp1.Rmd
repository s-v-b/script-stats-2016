---
title: "TP-1"
author: "SB"
date: "10 Sep 2015"
output: pdf_document
---

## Mortalité infantile et tabagisme

Le sujet de ce TP est emprunté à l'ouvrage [Statslab](https://www.stat.berkeley.edu/~statlabs/) de D. Nolan et T. Speed. Il s'agit d'une étude sur le poids des nouveaux nés. Cette étude s'inscrit dans une tentative générale de détermination des *causes* de la *mortalité infantile*. 

L'opinion générale des autorités sanitaires est que fumer durant la grossesse nuit à la santé du foetus et tend à accroitre la mortalité infantile. 

Les données utilisées dans ce TP proviennent d'une étude menée en Californie entre 1960 et 1967 auprès de mères suivies dans le cadre du 
*Kaiser Foundation Health Plan* (San Francisco–East Bay area). 

En décrivant les  15 000 familles impliquées dans l'étude, Yerushalmy écrit ([Yer64]): 

> The women seek medical care at Kaiser relatively early in pregnancy. Two-thirds report in the first trimester; nearly one-half when they are pregnant for
2 months or less. The study families represent a broad range in economic, social and educational characteristics. Nearly two-thirds are white, one-fifth negro, 3 to 4 percent oriental, and the remaining are members of other races and of mixed marriages. Some 30 percent of the husbands are in professional occupations. A large number are members of various unions. Nearly 10 percent are employed by the University of California at Berkeley in academic and administrative posts, and 20 percent are in government service. The educational level is somewhat higher than that of California as a whole, as is the average income. Thus, the study population is broadly based and is not atypical of an employed population. It is deficient in the indigent and the very affluent segments of the population since these groups are not likely to be represented in a prepaid medical program

```{r chargerPaquets}
libs <- c("ggplot2","knitr","plyr", "dplyr","magrittr")
result <-plyr::laply(.data=libs,.fun=require,character.only=TRUE,quietly=TRUE,warn.conflicts=FALSE)
```

La première étape de l'étude consiste à charger les données issues de l'étude. Celles-ci
se trouvent dans les fichiers `babiesI.csv`  et `babiesII.csv`
```{r chargerDonnees}
babiesI <- read.csv(file="../data/babiesI.csv", sep=",", header=TRUE)
babiesII <- read.table(file="../data/babiesII.csv", sep=",", header = TRUE)
```

On peut examiner le résultat du chargement 
```{r}
head(babiesI)
```
Les deux objets `babiesI,babiesII`  sont des tableaux à deux dimensions appelés `data.frame`. Chaque ligne correspond à un *individu* (un élément de l'*échantillon*, ici un bébé) et chqaue colonne à une variable. Dans le tableau `babiesI`, on ne dispose que de deux variables `bwt` (le poids à la naissance exprimés en *ounce = 28.3495231 grams*), `smoke` qui vaut `1` si la mère fumait durant la grossesse, `0` sinon. 

```{r conversion, echo=FALSE}
grams_ounce <- 28.3495231
```

Dans nos tableaux, on ne trouve que  1236 bébés, des garçons nés durant une année de l'étude, de grossesses simples et  qui ont vécu au moins 28 jours. 

On peut tout d'abord examiner la distribution des poids à la naissance selon le statut (fumeur ou pas) de la mère.


On peut se faire une idée rapide des distributions des poids selon le statut à l'aide des *boites à moustaches* (boxplots). 
```{r moustaches}
ggplot2::qplot(data=babiesI, y=bwt*grams_ounce, x=as.factor(smoke), geom = "boxplot")+ylab("Poids en grammes")+xlab("Statut")
```

1. Que désigne le trait plein situé à l'intérieur des boites ?
2. Que désignent les extrémités des boites ?
3. Comment sont calculées les longueurs des moustaches ?
4. Que désignent les points situés au delà des moustaches ?

1. Montrer que pour toute distribution de probabilité, l'écart entre espérance (si définie) et médiane est inférieur ou égal à l'écart-type.
2. Majorer l'écart inter-quartile par un mutiple de l'écart-type. Votre majoration est-elle tendue ? 

### Comment interpréter les boites à moustache ?


Les effectifs correspondants aux différents statuts se calculent facilement à l'aide de la fonction `table`
```{r effectifs}
table(babiesI$smoke)
```


[Documentation du paquet graphique](http://docs.ggplot2.org/current/)  `ggplot` (grammar of graphics). Pour les besoins de ce TP, on se contentera de travailler avec `qplot` (comme quick plot). 

On peut visualiser plus précisément la distrobution des poids à la naissance à l'aide d'histogrammes.

Un premier histogramme décrit la distribution tous statuts confondus.
```{r histo}
ggplot2::qplot(data = babiesI, x=bwt, geom="histogram") 
```
 
Jouez avec `binwidth` pour déformer l'histogramme.

Les histogrammes sont les plus simples des *estimateurs de densité*. Le choix de `binwidth` correspond à un choix de *largeur de bande*  dans le cas général. 
 
```{r histo-bin, eval=FALSE}
ggplot2::qplot(data = babiesI, x=bwt, geom="histogram",binwidth=5) 
``` 

Pour visualiser plus précisément ce qui distingue les deux groupes, on peut réaliser un histogramme par groupe à l'aide de `facet_grid`.  

```{r histos_facettes}
ggplot2::qplot(data = babiesI, x=bwt, geom="histogram") + ggplot2::facet_grid(facets=. ~ smoke)
```

Plutôt que les comptages, difficiles à comparer, on peut visualiser les fréquences relatives (attention en anglais *frequency* désigne un comptage).

```{r histos_facettes_filtrees}
ggplot2::qplot(data = dplyr::filter(babiesI, smoke %in% c(0,1)) , x=bwt*grams_ounce, y = ..density.. , geom="histogram") + ggplot2::facet_grid(facets=. ~ smoke) + xlab("Poids en grammes") +ylab("")
```

Est ce la manière la plus efficace de visualiser d'éventuelles différences ? 

L'approche que nous suivons a été appelée récemment *split-apply-combine*. On découpe l'échantillon en groupes (selon le statut de la mère), on applique à chaque groupe un traitement statistique ou graphique, et on combine les résultats. 

La paquet  [dplyr](https://cran.rstudio.com/web/packages/dplyr/vignettes/introduction.html) facilite grandement cette démarche (qui mobilise beaucoup d'énergie dans le domaine des *data science*). 

On groupe les données en fonction du statut.
```{r groupBy}
by_smoke <- dplyr::filter(.data=babiesI, smoke %in% c(0,1)) %>% dplyr::group_by(smoke)
```


A partir du regroupement, on peut facilement calculer des statistiques par groupe à l'aide de `summarise`. 
```{r resumes}
summary_by_smoke <- dplyr::summarise(.data=by_smoke, count=n(), med=median(bwt), mean=mean(bwt), iqr=IQR(bwt),  sd=sd(bwt))
```

```{r}
knitr::kable(summary_by_smoke)

```

### `babiesII` : des données enrichies

L'objet `babiesII` est un data.frame comme `babiesI`, il comporte plus de variables et le même nombre de lignes.  
```{r}
knitr::kable(head(babiesII))
```

- `gestation` désigne la durée de la grossesse en jours;
- `parity` vaut $0$ en cas de grossesse simple, 1 sinon;
- `age` désigne l'âge de la mère en années;
- `height` désigne la taille de la mère en pouces (1 inch = 0.0254 m)
- `weight` désigne le poids de la mère en livres (1 pound = 0.45359237 kg)

Visualisez le nuage de points défini par les coordonnées `height` et `bwt` dans `babiesII`, éliminez les valeurs aberrantes de `height`. Supperposer une courbe de régression non-paramétrique à l'aide de `geom_smooth`. 

Dessinez les histogrammes des tailles, poids et ages  en fonction du statut fumeur/non-fumeur de la mère. 

```{r histopoids}

```

```{r histotaille}

```

Fonctions de répartition des taille, poids et ages


`qqplot` pour durée de gestation (après filtrage de quelques données aberrantes)  
```{r  qqgestation}
ggplot2::qplot(data=dplyr::filter(babiesII,gestation<500 & smoke<=1), sample=gestation, stat="qq", color=as.factor(smoke))
```

### Durée de la gestation, poids à la naissance, statut

```{r}
ggplot2::qplot(data=dplyr::filter(babiesII,gestation<500 & smoke<=1), x=gestation,y=bwt,color=as.factor(smoke))
```



Résumez numériquement les distributions des tailles 

###  Tabac et grossesses multiples ? 

Construire la *table de contingence* `parity/smoke` (`parity`  en ligne, `smoke` en colonne)

```{r contingence}
dplyr::select(.data=babiesII,parity,smoke) %>% table %>% knitr::kable()
```

Visualisez cette table de contingence.

A l'aide d'un `mosaicplot`

```{r contingencemosaic}
dplyr::select(.data=babiesII,smoke,parity) %>% table %>% mosaicplot()
```

A l'aide d'un `barplot`

```{r contingencebar}
ggplot2::qplot(data=babiesII,fill=factor(parity),x=factor(smoke),geom="bar")
```

Un test de type chi-deux ($\chi^2$) conduit à ne pas rejeter l'hypothèse d'indépendance entre les deux variables.
```{r qui2}
chisq.test(dplyr::select(.data=babiesII,smoke,parity) %>% dplyr::filter(smoke %in% c(0,1)) %>% table)
```



### Statut et taille


```{r scatter}
ggplot2::qplot(data=dplyr::filter(babiesII,height<80, smoke %in% c(0,1)) , x= height,y = bwt)+ggplot2::geom_smooth()+ggplot2::facet_grid(facets= .~ smoke)
```


### Un premier test statistique

Nous pouvons calculer la statistique de Kolmogorov-SMirnov pour comparer les distributions des poids dans les deux groupes.

```{r}
ks.test(x=dplyr::filter(.data=babiesI, smoke==0)[,"bwt"],
        y=dplyr::filter(.data=babiesI, smoke==1)[,"bwt"])
```
Les interprétations habituelles conduisent à rejeter l'hypothèse selon laquelle les deux sous-échantillons proviendraient de tirages indépendants et identiquement distribués selon une même distribution.

*Ici, compte tenu des collisions, un test de ramg serait peut-être plus convenable ?*

> Compare the frequency, or incidence, of low-birth-weight babies for the two groups. How reliable do you think your estimates are? That is, how would the incidence of low birth weight change if a few more or fewer babies were classified as low birth weight? Assess the importance of the differences you found in your three types of comparisons (numerical, graphical, incidence).


## Tabac et mortalité infantile 


## Le poids à la naissance est il important pour la santé de l'enfant ?


[dplyr](https://cran.rstudio.com/web/packages/dplyr/vignettes/introduction.html)


> If a difference is found between the birth weights of babies born to smokers and those born to nonsmokers, the question of whether the difference is important to the health and development of the babies needs to be addressed.


> In analyzing the pregnancy outcomes from the CHDS, Yerushalmy ([Yer71]) found that although low birth weight is associated with an increase in the number of babies who die shortly after birth, the babies of smokers tended to have much lower death rates than the babies of nonsmokers. His calculations appear in Ta- ble 1.2. Rather than compare the overall mortality rate of babies born to smokers against the rate for babies born to nonsmokers, he made comparisons for smaller groups of babies. The babies were grouped according to their birth weight; then, within each group, the numbers of babies that died in the first 28 days after birth for smokers and nonsmokers were compared. To accommodate the different numbers of babies in the groups, rates instead of counts are used in making the comparisons.


> As in the comparison of Norwegian and American babies (New York Times, Mar. 1, 1995), in order to compare the mortality rates of babies born to smokers and those born to nonsmokers, Wilcox and Russell ([WR86]) and Wilcox ([Wil93]) advocate grouping babies according to their relative birth weights.



Si vous souhaitez continuer:  
[Child Health and Developmental Studies](http://www.chdstudies.org/)

